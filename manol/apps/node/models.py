from django.db import models

class Node(models.Model):
    ACCESS = (
        ("publickey","Public Key"),
        ("ssh","SSH")
    )
    
    name = models.CharField(max_length=255)
    ip = models.IPAddressField()
    is_active = models.BooleanField()
    access_method = models.CharField(max_length=10,choices=ACCESS)
    username = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    
    def __unicode__(self):
        return self.name

