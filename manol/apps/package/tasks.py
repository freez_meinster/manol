from time import sleep
from StringIO import StringIO
from fabric.api import env,run
from fabric.operations import put
from celery import Celery
from celery.utils.log import get_task_logger
from package.models import ItemLog
from datetime import datetime

app = Celery(broker="amqp://")

logger = get_task_logger(__name__)

def __run_script(host,password):
    env.host_string=host
    env.warn_only = True
    env.password = password
    env.shell = "/bin/sh -c"
    return run("set -m; /tmp/manol_script.sh")

def __construct_script(host,password,commands):
    env.warn_only = True
    env.host_string=host
    env.password = password
    put(StringIO("\n".join(commands)),"/tmp/manol_script.sh", mode=0755)

@app.task
def send_command(command_list,deploy):
    logger.info("Send command start")
    command_list.insert(0,"set -x")
    command_list.insert(0,"#!/usr/bin/env bash")
    command_list.append("\n exit 0")
    deploy.is_running = True
    deploy.save()
    for node in deploy.node.all() :
        ilog = ItemLog(
            command_list = "\n".join(command_list),
            node = node,
            output = ""
        )
        ilog.save()
        host_string = "%s@%s" % (node.username,node.ip)
        __construct_script(host_string,node.password,command_list)
        out = __run_script(host_string,node.password)
        ilog.output = out
        ilog.time_end = datetime.today()
        ilog.save()
    deploy.is_running = False
    deploy.save()
    logger.info("Send Command finish")
