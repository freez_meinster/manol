from django.template import Context, Template
from django.http import HttpResponse
from django.shortcuts import render_to_response, redirect
from package.models import Deploy, DeployStep
from package import tasks

def __translate_command(deploy,command):
    t = Template(command)
    context = {"deploy" : deploy }
    for env in deploy.deployenv_set.all():
        context[env.key] = env.value
    c = Context(context)
    return t.render(c).split("\n")

def landing(request):
    return render_to_response("landing.html")


def index(request):
    deploy = Deploy.objects.filter(is_active=True)
    return render_to_response("package/index.html",{
        "deploy" : deploy
    })

def __get_ops_dir(deploy):
    cmd = []
    for env in deploy.deployenv_set.all():
        if env.key == "ops_dir" :
           cmd.append("cd %s" % env.value )
    return cmd

def deploy(request,id):
    
    deploy = Deploy.objects.get(id=id)
    if deploy.is_running :
         return HttpResponse("Deployment already running")
    else :
        steps = [] 
        steps.extend(__get_ops_dir(deploy))
        for step in deploy.deploystep_set.all().filter(is_active=True).order_by("position"):
            if step.command :
                for temp in step.command.all():
                    steps.extend(__translate_command(deploy,temp.command.replace("\r", "")))
            if step.aux_command :
                steps.extend(__translate_command(deploy,step.aux_command.replace("\r", "")))
        tasks.send_command.delay(steps,deploy)
        return render_to_response("package/deploy.html",{
            'deploy' : deploy,
            'step_list' : steps
        })
