import models
from django.contrib import admin

class ItemLogAdmin(admin.ModelAdmin):
    list_display = ['command_list','node','time_start','time_end']

admin.site.register(models.Deploy)
admin.site.register(models.DeployStep)
admin.site.register(models.DeployEnv)
admin.site.register(models.DeployStepTemplate)
admin.site.register(models.ItemLog,ItemLogAdmin)