# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Deploy.repo_url'
        db.alter_column(u'package_deploy', 'repo_url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True))

        # Changing field 'Deploy.repo_source'
        db.alter_column(u'package_deploy', 'repo_source', self.gf('django.db.models.fields.CharField')(max_length=255, null=True))

    def backwards(self, orm):

        # Changing field 'Deploy.repo_url'
        db.alter_column(u'package_deploy', 'repo_url', self.gf('django.db.models.fields.URLField')(default='', max_length=200))

        # Changing field 'Deploy.repo_source'
        db.alter_column(u'package_deploy', 'repo_source', self.gf('django.db.models.fields.CharField')(default='', max_length=255))

    models = {
        u'package.deploy': {
            'Meta': {'object_name': 'Deploy'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'repo_source': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'repo_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'package.deploystep': {
            'Meta': {'object_name': 'DeployStep'},
            'command': ('django.db.models.fields.TextField', [], {}),
            'deploy': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['package.Deploy']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'package.deploystepenv': {
            'Meta': {'object_name': 'DeployStepEnv'},
            'deploy_step': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['package.DeployStep']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'package.itemlog': {
            'Meta': {'object_name': 'ItemLog'},
            'deploy_step': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['package.DeployStep']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'output': ('django.db.models.fields.TextField', [], {}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'time_start': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'time_stop': ('django.db.models.fields.DateTimeField', [], {})
        }
    }

    complete_apps = ['package']