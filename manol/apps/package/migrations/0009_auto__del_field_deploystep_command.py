# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'DeployStep.command'
        db.delete_column(u'package_deploystep', 'command_id')

        # Adding M2M table for field command on 'DeployStep'
        m2m_table_name = db.shorten_name(u'package_deploystep_command')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('deploystep', models.ForeignKey(orm[u'package.deploystep'], null=False)),
            ('deploysteptemplate', models.ForeignKey(orm[u'package.deploysteptemplate'], null=False))
        ))
        db.create_unique(m2m_table_name, ['deploystep_id', 'deploysteptemplate_id'])


    def backwards(self, orm):
        # Adding field 'DeployStep.command'
        db.add_column(u'package_deploystep', 'command',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['package.DeployStepTemplate'], null=True, blank=True),
                      keep_default=False)

        # Removing M2M table for field command on 'DeployStep'
        db.delete_table(db.shorten_name(u'package_deploystep_command'))


    models = {
        u'node.node': {
            'Meta': {'object_name': 'Node'},
            'access_method': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'package.deploy': {
            'Meta': {'object_name': 'Deploy'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'node': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['node.Node']", 'symmetrical': 'False'}),
            'repo_access_type': ('django.db.models.fields.CharField', [], {'default': "'https'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'repo_auth_password': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'repo_auth_username': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'repo_source': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'repo_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'package.deploystep': {
            'Meta': {'object_name': 'DeployStep'},
            'aux_command': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'command': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['package.DeployStepTemplate']", 'null': 'True', 'blank': 'True'}),
            'deploy': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['package.Deploy']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'package.deploystepenv': {
            'Meta': {'object_name': 'DeployStepEnv'},
            'deploy_step': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['package.DeployStep']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'value': ('django.db.models.fields.TextField', [], {})
        },
        u'package.deploysteptemplate': {
            'Meta': {'object_name': 'DeployStepTemplate'},
            'command': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'package.itemlog': {
            'Meta': {'object_name': 'ItemLog'},
            'deploy_step': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['package.DeployStep']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'output': ('django.db.models.fields.TextField', [], {}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'time_start': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'time_stop': ('django.db.models.fields.DateTimeField', [], {})
        }
    }

    complete_apps = ['package']