# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'ItemLog.time_stop'
        db.delete_column(u'package_itemlog', 'time_stop')

        # Deleting field 'ItemLog.deploy_step'
        db.delete_column(u'package_itemlog', 'deploy_step_id')


    def backwards(self, orm):
        # Adding field 'ItemLog.time_stop'
        db.add_column(u'package_itemlog', 'time_stop',
                      self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True),
                      keep_default=False)


        # User chose to not deal with backwards NULL issues for 'ItemLog.deploy_step'
        raise RuntimeError("Cannot reverse this migration. 'ItemLog.deploy_step' and its values cannot be restored.")
        
        # The following code is provided here to aid in writing a correct migration        # Adding field 'ItemLog.deploy_step'
        db.add_column(u'package_itemlog', 'deploy_step',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['package.DeployStep']),
                      keep_default=False)


    models = {
        u'node.node': {
            'Meta': {'object_name': 'Node'},
            'access_method': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'package.deploy': {
            'Meta': {'object_name': 'Deploy'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'node': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['node.Node']", 'symmetrical': 'False'}),
            'repo_access_type': ('django.db.models.fields.CharField', [], {'default': "'https'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'repo_auth_password': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'repo_auth_username': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'repo_source': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'repo_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'package.deployenv': {
            'Meta': {'object_name': 'DeployEnv'},
            'deploy': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['package.Deploy']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'value': ('django.db.models.fields.TextField', [], {})
        },
        u'package.deploystep': {
            'Meta': {'object_name': 'DeployStep'},
            'aux_command': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'command': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['package.DeployStepTemplate']", 'null': 'True', 'blank': 'True'}),
            'deploy': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['package.Deploy']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'position': ('django.db.models.fields.IntegerField', [], {})
        },
        u'package.deploysteptemplate': {
            'Meta': {'object_name': 'DeployStepTemplate'},
            'command': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'package.itemlog': {
            'Meta': {'object_name': 'ItemLog'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'output': ('django.db.models.fields.TextField', [], {}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'time_start': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['package']