# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'DeployStepTemplate'
        db.create_table(u'package_deploysteptemplate', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('command', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'package', ['DeployStepTemplate'])

        # Adding field 'DeployStep.aux_command'
        db.add_column(u'package_deploystep', 'aux_command',
                      self.gf('django.db.models.fields.TextField')(null=True, blank=True),
                      keep_default=False)


        # Renaming column for 'DeployStep.command' to match new field type.
        db.rename_column(u'package_deploystep', 'command', 'command_id')
        # Changing field 'DeployStep.command'
        db.alter_column(u'package_deploystep', 'command_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['package.DeployStepTemplate']))
        # Adding index on 'DeployStep', fields ['command']
        db.create_index(u'package_deploystep', ['command_id'])


    def backwards(self, orm):
        # Removing index on 'DeployStep', fields ['command']
        db.delete_index(u'package_deploystep', ['command_id'])

        # Deleting model 'DeployStepTemplate'
        db.delete_table(u'package_deploysteptemplate')

        # Deleting field 'DeployStep.aux_command'
        db.delete_column(u'package_deploystep', 'aux_command')


        # Renaming column for 'DeployStep.command' to match new field type.
        db.rename_column(u'package_deploystep', 'command_id', 'command')
        # Changing field 'DeployStep.command'
        db.alter_column(u'package_deploystep', 'command', self.gf('django.db.models.fields.TextField')())

    models = {
        u'package.deploy': {
            'Meta': {'object_name': 'Deploy'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'repo_access_type': ('django.db.models.fields.CharField', [], {'default': "'https'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'repo_auth_password': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'repo_auth_username': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'repo_source': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'repo_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'package.deploystep': {
            'Meta': {'object_name': 'DeployStep'},
            'aux_command': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'command': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['package.DeployStepTemplate']"}),
            'deploy': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['package.Deploy']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'package.deploystepenv': {
            'Meta': {'object_name': 'DeployStepEnv'},
            'deploy_step': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['package.DeployStep']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'value': ('django.db.models.fields.TextField', [], {})
        },
        u'package.deploysteptemplate': {
            'Meta': {'object_name': 'DeployStepTemplate'},
            'command': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'package.itemlog': {
            'Meta': {'object_name': 'ItemLog'},
            'deploy_step': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['package.DeployStep']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'output': ('django.db.models.fields.TextField', [], {}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'time_start': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'time_stop': ('django.db.models.fields.DateTimeField', [], {})
        }
    }

    complete_apps = ['package']