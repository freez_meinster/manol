# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Deploy'
        db.create_table(u'package_deploy', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('repo_source', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('repo_url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('is_active', self.gf('django.db.models.fields.BooleanField')()),
        ))
        db.send_create_signal(u'package', ['Deploy'])

        # Adding model 'DeployStep'
        db.create_table(u'package_deploystep', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('deploy', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['package.Deploy'])),
            ('command', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'package', ['DeployStep'])

        # Adding model 'DeployStepEnv'
        db.create_table(u'package_deploystepenv', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('deploy_step', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['package.DeployStep'])),
        ))
        db.send_create_signal(u'package', ['DeployStepEnv'])

        # Adding model 'ItemLog'
        db.create_table(u'package_itemlog', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('deploy_step', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['package.DeployStep'])),
            ('state', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('output', self.gf('django.db.models.fields.TextField')()),
            ('time_start', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('time_stop', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'package', ['ItemLog'])


    def backwards(self, orm):
        # Deleting model 'Deploy'
        db.delete_table(u'package_deploy')

        # Deleting model 'DeployStep'
        db.delete_table(u'package_deploystep')

        # Deleting model 'DeployStepEnv'
        db.delete_table(u'package_deploystepenv')

        # Deleting model 'ItemLog'
        db.delete_table(u'package_itemlog')


    models = {
        u'package.deploy': {
            'Meta': {'object_name': 'Deploy'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'repo_source': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'repo_url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        u'package.deploystep': {
            'Meta': {'object_name': 'DeployStep'},
            'command': ('django.db.models.fields.TextField', [], {}),
            'deploy': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['package.Deploy']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'package.deploystepenv': {
            'Meta': {'object_name': 'DeployStepEnv'},
            'deploy_step': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['package.DeployStep']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'package.itemlog': {
            'Meta': {'object_name': 'ItemLog'},
            'deploy_step': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['package.DeployStep']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'output': ('django.db.models.fields.TextField', [], {}),
            'state': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'time_start': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'time_stop': ('django.db.models.fields.DateTimeField', [], {})
        }
    }

    complete_apps = ['package']