# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'ItemLog.time_end'
        db.add_column(u'package_itemlog', 'time_end',
                      self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 7, 3, 0, 0)),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'ItemLog.time_end'
        db.delete_column(u'package_itemlog', 'time_end')


    models = {
        u'node.node': {
            'Meta': {'object_name': 'Node'},
            'access_method': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'ip': ('django.db.models.fields.IPAddressField', [], {'max_length': '15'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'username': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'package.deploy': {
            'Meta': {'object_name': 'Deploy'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {}),
            'is_running': ('django.db.models.fields.BooleanField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'node': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['node.Node']", 'symmetrical': 'False'}),
            'repo_access_type': ('django.db.models.fields.CharField', [], {'default': "'https'", 'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'repo_auth_password': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'repo_auth_username': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'repo_source': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'}),
            'repo_url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'package.deployenv': {
            'Meta': {'object_name': 'DeployEnv'},
            'deploy': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['package.Deploy']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'value': ('django.db.models.fields.TextField', [], {})
        },
        u'package.deploystep': {
            'Meta': {'object_name': 'DeployStep'},
            'aux_command': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'command': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['package.DeployStepTemplate']", 'null': 'True', 'blank': 'True'}),
            'deploy': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['package.Deploy']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {}),
            'position': ('django.db.models.fields.IntegerField', [], {})
        },
        u'package.deploysteptemplate': {
            'Meta': {'object_name': 'DeployStepTemplate'},
            'command': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'package.itemlog': {
            'Meta': {'object_name': 'ItemLog'},
            'command_list': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'node': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['node.Node']"}),
            'output': ('django.db.models.fields.TextField', [], {}),
            'time_end': ('django.db.models.fields.DateTimeField', [], {}),
            'time_start': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['package']