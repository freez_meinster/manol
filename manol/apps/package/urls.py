from django.conf.urls import patterns, include, url

urlpatterns = patterns('package.views',
    url(r'^$', 'index', name='package_index'), 
    url(r'^deploy/(?P<id>\d+)/$', 'deploy', name='package_deploy'),                   
)