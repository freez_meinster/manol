from node.models import Node
from django.db import models

class Deploy(models.Model):
    REPO = (
        ("github", "Github"),
        ("bitbucket", "Bitbucket")
    )
    
    REPO_TYPE = (
        ("ssh", "SSH"),
        ('https', "HTTPS"),
        ('git', "Git")
    )
    
    name = models.CharField(max_length=255)
    repo_source = models.CharField(max_length=255, choices=REPO, blank=True, null=True)
    repo_url = models.URLField(blank=True, null=True)
    repo_access_type = models.CharField(max_length=255,
                                        choices=REPO_TYPE,
                                        blank=True,
                                        null=True,
                                        default="https")
    repo_auth_username = models.CharField(max_length=255, blank=True, null=True)
    repo_auth_password = models.CharField(max_length=255, blank=True, null=True)
    is_active = models.BooleanField()
    is_running = models.BooleanField()
    node = models.ManyToManyField(Node)
    
    def __unicode__(self):
        return self.name

class DeployStep(models.Model):
    deploy = models.ForeignKey("Deploy")
    command = models.ManyToManyField("DeployStepTemplate",blank=True, null=True)
    aux_command = models.TextField(blank=True, null=True)
    position = models.IntegerField()
    is_active = models.BooleanField()
    
    def __unicode__(self):
        return "Deploy %s step %s" % (self.deploy.name, str(self.position))
    
class DeployStepTemplate(models.Model):
    name = models.CharField(max_length=255)
    command = models.TextField()
    
    def __unicode__(self):
        return self.name
    
class DeployEnv(models.Model):
    deploy = models.ForeignKey("Deploy")
    key = models.CharField(max_length=255)
    value = models.TextField()
    
    def __unicode__(self):
        return self.key
    

class ItemLog(models.Model):
    command_list = models.TextField()
    node = models.ForeignKey(Node)
    output = models.TextField()
    time_start = models.DateTimeField(auto_now_add=True)
    time_end = models.DateTimeField(blank=True, null=True)
    
