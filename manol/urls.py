from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'package.views.landing', name='landing'),
    url(r'^package/', include('package.urls')),

    url(r'^admin/', include(admin.site.urls)),
)
